// ./navigation/TabNavigator.js

import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {
  MainStackNavigator,
  ContactStackNavigator,
  QuranStackNavigator,
  HisabStackNavigator,
  PrayerTimesStackNavigator,
} from './StackNavigator';
import {Quran} from '../screens/Quran';

const Tab = createBottomTabNavigator();

const BottomTabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Home" component={MainStackNavigator} />
      <Tab.Screen name="Quran" component={QuranStackNavigator} />
      <Tab.Screen name="Hisab" component={HisabStackNavigator} />
      <Tab.Screen name="Prayer Times" component={PrayerTimesStackNavigator} />
    </Tab.Navigator>
  );
};

export default BottomTabNavigator;
