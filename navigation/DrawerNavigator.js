// ./navigation/DrawerNavigator.js

// import React from 'react';

// import {createDrawerNavigator} from '@react-navigation/drawer';

// import {ContactStackNavigator} from './StackNavigator';
// import TabNavigator from './TabNavigator';

// const Drawer = createDrawerNavigator();

// const DrawerNavigator = () => {
//   return (
//     <Drawer.Navigator>
//       {/* <Drawer.Screen name="Home" component={TabNavigator} /> */}
//       <Drawer.Screen name="Contact" component={ContactStackNavigator} />
//     </Drawer.Navigator>
//   );
// };

// export default DrawerNavigator;

// ./navigation/DrawerNavigator.js

import React from 'react';

import {createDrawerNavigator} from '@react-navigation/drawer';

import {ContactStackNavigator, HisabStackNavigator} from './StackNavigator';
import TabNavigator from './TabNavigator';

const Drawer = createDrawerNavigator();

const DrawerNavigator = () => {
  return (
    <Drawer.Navigator
      hideStatusBar={true}
      screenOptions={{
        headerShown: false,
      }}>
      <Drawer.Screen name="Home" component={TabNavigator} />
      <Drawer.Screen name="Hisab" component={HisabStackNavigator} />

      <Drawer.Screen name="Contact" component={ContactStackNavigator} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
