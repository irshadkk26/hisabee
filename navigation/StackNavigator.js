// ./navigation/StackNavigator.js

import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Home from '../screens/Home';
import Hisab from '../screens/Hisab';
import HisabHome from '../screens/hisab/HisabHome';
import SetTarget from '../screens/hisab/SetTarget';
import About from '../screens/About';
import Contact from '../screens/Contact';
import Quran from '../screens/Quran/Quran';
import Surah from '../screens/Surah/Surah';
import NamazTiming from '../screens/NamazTiming';

const Stack = createStackNavigator();

const screenOptionStyle = {
  headerStyle: {
    backgroundColor: '#a39889',
  },
  headerTintColor: 'white',
  headerBackTitle: 'Back',
};

const MainStackNavigator = () => {
  return (
    // <Stack.Navigator screenOptions={screenOptionStyle}>
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="About" component={About} />
    </Stack.Navigator>
  );
};

const ContactStackNavigator = () => {
  return (
    // <Stack.Navigator screenOptions={screenOptionStyle}>
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Contact" component={Contact} />
    </Stack.Navigator>
  );
};
const PrayerTimesStackNavigator = () => {
  return (
    // <Stack.Navigator screenOptions={screenOptionStyle}>
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="PrayerTimes" component={NamazTiming} />
    </Stack.Navigator>
  );
};
const QuranStackNavigator = () => {
  return (
    // <Stack.Navigator screenOptions={screenOptionStyle}>
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Quran" component={Quran} />
      <Stack.Screen name="Surah" component={Surah} />
    </Stack.Navigator>
  );
};
const HisabStackNavigator = () => {
  return (
    // <Stack.Navigator screenOptions={screenOptionStyle}>
    <Stack.Navigator screenOptions={screenOptionStyle}>
      <Stack.Screen name="Hisab" component={Hisab} />
      <Stack.Screen name="Categories" component={HisabHome} />
      <Stack.Screen name="SetTarget" component={SetTarget} />
    </Stack.Navigator>
  );
};

export {
  MainStackNavigator,
  ContactStackNavigator,
  QuranStackNavigator,
  HisabStackNavigator,
  PrayerTimesStackNavigator,
};
