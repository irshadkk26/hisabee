import React from 'react';
import {NavigationContainer, DefaultTheme} from '@react-navigation/native';

import DrawerNavigator from './navigation/DrawerNavigator';
// import BottomTabNavigator from './navigation/TabNavigator';
// import ScrollMore from './components/ScrollMore';
// import Quran from './components/Quran/Quran';

import {Provider} from 'react-redux';
import store from './redux/store';
import {withAuthenticator} from 'aws-amplify-react-native';

const MyTheme = {
  dark: false,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
    notification: 'rgb(255, 69, 58)',
  },
};

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer theme={MyTheme}>
        <DrawerNavigator />
      </NavigationContainer>
    </Provider>
    // <Provider store={store}>
    //   <Quran></Quran>
    // </Provider>
  );
};
export default withAuthenticator(App);
