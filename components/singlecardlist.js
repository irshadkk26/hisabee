import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
  Radio,
  Switch,
} from 'native-base';
import {Image} from 'react-native';

export default class SingleCardList extends Component {
  render() {
    const items = this.props.listData;
    console.log('=======items====');
    console.log(items);
    console.log('======items=====');
    return (
      <Container>
        {/* <Header>
          <Text>{this.props.header}</Text>
        </Header> */}
        <Content>
          {/* <Card>
            <CardItem header>
              <Text>NativeBase</Text>
            </CardItem>
            <CardItem>
              <Body>
                <View style={{display: 'flex', flexDirection: 'row'}}>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>jamaath</Text>
                  </View>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>no jama'ath</Text>
                  </View>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>no</Text>
                  </View>
                </View>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>GeekyAnts</Text>
            </CardItem>
          </Card> */}
          {items.map((item, index) => {
            return (
              <Card key={index}>
                <CardItem>
                  <Left>
                    <Thumbnail source={require('../images/hisabee.png')} />
                    <Body>
                      <Text style={{textTransform: 'capitalize'}}>
                        {item?.name}?
                      </Text>
                      <Text note>{item?.description}</Text>
                    </Body>
                  </Left>
                </CardItem>
                <CardItem cardBody>
                  <Image
                    source={require('../images/hisabee.png')}
                    style={{height: 200, width: null, flex: 1}}
                  />
                </CardItem>
                <CardItem
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-evenly',
                  }}>
                  <View>
                    <Button transparent>
                      <Switch value={true} />
                      <Text>Done</Text>
                    </Button>
                  </View>
                  <View>
                    <Button transparent>
                      <Switch value={false} />
                      <Text>Not Done</Text>
                    </Button>
                  </View>
                  {item.category === 'farz namaz' && (
                    <View>
                      <Button transparent>
                        <Switch value={false} />
                        <Text>Not Done</Text>
                      </Button>
                    </View>
                  )}
                </CardItem>
              </Card>
            );
          })}

          {/* <List>
            {items.map((item, index) => {
              console.log(item);
              return (
                <ListItem thumbnail key={index}>
                  <Left>
                    <Thumbnail
                      square
                      source={require('../images/hisabee.png')}
                    />
                  </Left>
                  <Body>
                    <Text>{item?.name}</Text>
                    <Text note numberOfLines={1}>
                      {item?.description}
                    </Text>
                    <View style={{display: 'flex', flexDirection: 'row'}}>
                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <CheckBox checked={true} />
                        <Text>jamaath</Text>
                      </View>
                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <CheckBox checked={true} />
                        <Text>no jama'ath</Text>
                      </View>
                      <View style={{display: 'flex', flexDirection: 'row'}}>
                        <CheckBox checked={true} />
                        <Text>no</Text>
                      </View>
                    </View>
                  </Body>
                  <Right>
                    <Button transparent>
                      <Text>View</Text>
                    </Button>
                  </Right>
                </ListItem>
              );
            })}
          </List> */}
        </Content>
      </Container>
    );
  }
}
