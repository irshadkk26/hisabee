export const scrollmore = (state = {}, action) => {
  switch (action.type) {
    // overwrite list items with updated results
    case 'UPDATE_LIST_RESULTS':
      return Object.assign({}, state, {
        surahlist: action.items,
      });
    case 'UPDATE_LIST_RESULTS_SURAH':
      return Object.assign({}, state, {
        surahayaths: action.items,
      });

    default:
      return state;
  }
};

export default scrollmore;
