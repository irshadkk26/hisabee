import {combineReducers} from 'redux';
import tasks from './task';
import {scrollmore} from './scrollmore';

export default combineReducers({tasks, scrollmore});
