// ./screens/Contact.js

import React from 'react';
import {StyleSheet, SafeAreaView, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
} from 'native-base';
const surahArr = [
  require('../resources/surah/surah_1.json'),
  require('../resources/surah/surah_2.json'),
  require('../resources/surah/surah_3.json'),
  require('../resources/surah/surah_4.json'),
  require('../resources/surah/surah_5.json'),
  require('../resources/surah/surah_6.json'),
];
const Surah = ({route, navigation}) => {
  const {sura} = route.params;

  const surahJson = surahArr[sura];
  const count = surahJson?.count;
  const verses = surahJson?.verse;
  const versesArr = [];
  for (const key in verses) {
    if (verses.hasOwnProperty(key)) {
      versesArr.push(verses[key]);
    }
  }
  return (
    <ScrollView>
      <List>
        {versesArr.map((item, index) => {
          return (
            <ListItem thumbnail key={index}>
              <Left>
                <Thumbnail square source={require('../images/hisabee.png')} />
              </Left>
              <Body>
                <Text style={{direction: 'ltr', textAlign: 'right'}}>
                  {item}
                </Text>
                {/* <Text note numberOfLines={1}>
                  {item.title}
                </Text> */}
              </Body>
              <Right>
                <Button transparent>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          );
        })}
      </List>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default Surah;
