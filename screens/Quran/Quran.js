// import {fetchResults} from './surahlist';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {FlatList, SafeAreaView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
// import delay from 'delay';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
} from 'native-base';

export default function Quran({route, navigation}) {
  const dispatch = useDispatch();
  const listItems = useSelector((state) => state.scrollmore.surahlist);
  const totalItems = Array.isArray(listItems) ? listItems.length : 0;
  const [loadingMore, setLoadingMore] = useState(false);
  const [allLoaded, setAllLoaded] = useState(false);
  const RECORDS_PER_FETCH = 15;

  const surahlist = require('../../resources/surah.json');
  const fetchResults = (startingId = 0) => {
    let obj = [];

    for (let i = startingId; i < startingId + RECORDS_PER_FETCH; i++) {
      if (surahlist[i] === undefined) break;

      obj.push(surahlist[i]);
    }
    return obj;
  };

  useEffect(() => {
    initialiseList();
  }, []);

  const initialiseList = async () => {
    // this is done for testing purposes - reset AsyncStorage on every app refresh
    await AsyncStorage.removeItem('saved_list_quran');

    // get current persisted list items (will be null if above line is not removed)
    const curItems = await AsyncStorage.getItem('saved_list_quran');

    if (curItems === null) {
      // no current items in AsyncStorage - fetch initial items
      json = fetchResults(0);

      // set initial list in AsyncStorage
      await AsyncStorage.setItem('saved_list_quran', JSON.stringify(json));
    } else {
      // current items exist - format as a JSON object
      json = JSON.parse(curItems);
    }

    // update Redux store (Redux will ignore if `json` is same as current list items)
    dispatch({
      type: 'UPDATE_LIST_RESULTS',
      items: json,
    });
  };

  const persistResults = async (newItems) => {
    // get current persisted list items
    const curItems = await AsyncStorage.getItem('saved_list_quran');

    // format as a JSON object
    let json = curItems === null ? {} : JSON.parse(curItems);

    // add new items to json object
    for (let item of newItems) {
      json.push(item);
    }

    // persist updated item list
    await AsyncStorage.setItem('saved_list_quran', JSON.stringify(json));

    // update Redux store
    dispatch({
      type: 'UPDATE_LIST_RESULTS',
      items: json,
    });
  };

  const loadMoreResultsQuran = async (info) => {
    // if already loading more, or all loaded, return
    if (loadingMore || allLoaded) return;

    // set loading more (also updates footer text)
    setLoadingMore(true);

    // get next results
    const newItems = fetchResults(totalItems);

    // mimic server-side API request and delay execution for 1 second
    // await delay(1000);

    if (newItems.length === 0) {
      // if no new items were fetched, set all loaded to true to prevent further requests
      setAllLoaded(true);
    } else {
      // process the newly fetched items
      await persistResults(newItems);
    }

    // load more complete, set loading more to false
    setLoadingMore(false);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {listItems && (
        <FlatList
          // ListHeaderComponent={
          //   <View style={styles.header}>
          //     <Text style={styles.title}>Displaying {totalItems} Items</Text>
          //   </View>
          // }
          ListFooterComponent={
            <View>{loadingMore && <Text>Loading More...</Text>}</View>
          }
          scrollEventThrottle={250}
          onEndReached={(info) => {
            loadMoreResultsQuran(info);
          }}
          onEndReachedThreshold={0.01}
          data={listItems}
          keyExtractor={(item) => 'item_' + item.index}
          renderItem={({item, index}) => {
            return (
              <ListItem thumbnail key={index}>
                <Left>
                  <Thumbnail
                    square
                    source={require('../../images/hisabee.png')}
                  />
                </Left>
                <Body
                  style={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                  }}>
                  <Text numberOfLines={1}>{item.title}</Text>
                  <Text>{item.titleAr}</Text>
                </Body>
                <Right>
                  <Button
                    transparent
                    onPress={() =>
                      navigation.navigate('Surah', {
                        sura: index,
                      })
                    }>
                    <Text>View</Text>
                  </Button>
                </Right>
              </ListItem>
            );
          }}
        />
      )}
    </SafeAreaView>
  );
}
