// ./screens/Contact.js

import React from 'react';
import {StyleSheet, SafeAreaView, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
} from 'native-base';
import surahJson from '../resources/surah.json';

const Quran = ({navigation}) => {
  return (
    <ScrollView>
      <List>
        {surahJson.map((item, index) => {
          return (
            <ListItem thumbnail key={index}>
              <Left>
                <Thumbnail square source={require('../images/hisabee.png')} />
              </Left>
              <Body
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text numberOfLines={1}>{item.title}</Text>
                <Text>{item.titleAr}</Text>
              </Body>
              <Right>
                <Button
                  transparent
                  onPress={() =>
                    navigation.navigate('Surah', {
                      sura: index,
                    })
                  }>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          );
        })}
      </List>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default Quran;
