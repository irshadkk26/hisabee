// ./screens/Home.js

import React, {useEffect, useState} from 'react';
import {
  View,
  Button,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import SingleCardList from '../components/singlecardlist';
// import {fetchTodos} from '../services';

import {API, graphqlOperation} from 'aws-amplify';

import {listTasks} from '../src/graphql/queries';
import {
  createTodo,
  updateTodo,
  deleteTodo,
  createTask,
} from '../src/graphql/mutations';

const Home = () => {
  const [tasks, setTodos] = useState([]);
  useEffect(() => {
    fetchTodos();
    // createTasks();
  }, []);
  async function fetchTodos() {
    try {
      const todoData = await API.graphql(graphqlOperation(listTasks));
      // const todoData = await API.graphql(
      //   graphqlOperation(listTasks, {
      //     filter: {
      //       category: {
      //         eq: 'farz namaz',
      //       },
      //     },
      //   }),
      // );
      console.log('---------');
      console.log(todoData.data.listTasks.items);
      console.log('---------');
      const tasks = todoData.data.listTasks.items;
      setTodos(tasks);
    } catch (err) {
      console.log(err);
      console.log('error fetching tasks');
    }
  }

  async function createTasks() {
    // const task = [
    //   {
    //     category: 'sunnah namaz',
    //     description: 'before subhi',
    //     name: 'before subhi',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'before dhuhar namaz',
    //     name: 'before dhuhar',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'after dhuhar namaz',
    //     name: 'after dhuhar',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'before asar namaz',
    //     name: 'before asar',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'after maghrib namaz',
    //     name: 'after maghrib',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'before isha namaz',
    //     name: 'before isha',
    //     repeatingin: 'daily',
    //   },
    //   {
    //     category: 'sunnah namaz',
    //     description: 'after isha namaz',
    //     name: 'after isha',
    //     repeatingin: 'daily',
    //   },
    // ];

    const task = [
      {
        category: 'sunnah nomb',
        description: 'sunnah nomb',
        name: 'sunnah nomb',
        repeatingin: 'daily',
      },
      {
        category: 'farz nomb',
        description: 'farz nomb',
        name: 'farz nomb',
        repeatingin: 'yearly',
      },
      {
        category: 'swadaka',
        description: 'swadaka',
        name: 'swadaka',
        repeatingin: 'daily',
      },
      {
        category: 'zakath',
        description: 'zakath',
        name: 'zakath',
        repeatingin: 'monthly',
      },
      {
        category: 'paretns care',
        description: 'paretns care',
        name: 'did good for paretns',
        repeatingin: 'daily',
      },
      {
        category: 'paretns care',
        description: 'paretns care',
        name: 'did good for paretns',
        repeatingin: 'daily',
      },
      {
        category: 'wife care',
        description: 'wife care',
        name: 'did good for wife',
        repeatingin: 'daily',
      },
      {
        category: 'kids care',
        description: 'kids care',
        name: 'did good for kids',
        repeatingin: 'daily',
      },
      {
        category: 'neighbors care',
        description: 'neighbors care',
        name: 'did good for neighbors',
        repeatingin: 'daily',
      },
      {
        category: 'soceity care',
        description: 'soceity care',
        name: 'did good for soceity',
        repeatingin: 'daily',
      },
      {
        category: 'quran',
        description: 'quran read',
        name: 'read the quran',
        repeatingin: 'daily',
      },
      {
        category: 'quran',
        description: 'quran byheart',
        name: 'byheart the quran',
        repeatingin: 'daily',
      },
      {
        category: 'quran',
        description: 'study byheart',
        name: 'study the quran',
        repeatingin: 'daily',
      },
      {
        category: 'quran',
        description: 'practice byheart',
        name: 'practice the quran',
        repeatingin: 'daily',
      },
    ];
    task.forEach(async (element) => {
      await API.graphql(graphqlOperation(createTask, {input: element}));
    });
  }

  if (tasks.length > 0) {
    // const fardhNamazes = [];
    // tasks.forEach((item) => {
    //   if (item.category === 'farz namaz') {
    //     fardhNamazes.push(item);
    //   }
    // });
    // const sunnahNamazes = [];
    // tasks.map((item) => {
    //   if (item.category === 'sunnah namaz') {
    //     sunnahNamazes.push(item);
    //   }
    // });
    return <SingleCardList listData={tasks} header="IBADATHS"></SingleCardList>;
  } else {
    return <Text>''</Text>;
  }
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default Home;
