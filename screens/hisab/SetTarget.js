// ./screens/Home.js

import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
  Radio,
  Switch,
} from 'native-base';

import {API, graphqlOperation} from 'aws-amplify';

import {listTasks} from '../../src/graphql/queries';
import {
  createTodo,
  updateTodo,
  deleteTodo,
  createTask,
} from '../../src/graphql/mutations';
const _ = require('lodash');

const SetTarget = ({navigation}) => {
  const [tasks, setTodos] = useState([]);
  const [categories, setCategories] = useState([]);

  return (
    <View>
      <Text>this is my page</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default SetTarget;
