// ./screens/Home.js

import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
  Radio,
  Switch,
} from 'native-base';

import {API, graphqlOperation} from 'aws-amplify';

import {listTasks} from '../../src/graphql/queries';
import {
  createTodo,
  updateTodo,
  deleteTodo,
  createTask,
} from '../../src/graphql/mutations';
const _ = require('lodash');

const Hisabhome = ({navigation}) => {
  const [tasks, setTodos] = useState([]);
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    fetchTodos();
    // createTasks();
  }, []);
  async function fetchTodos() {
    try {
      const todoData = await API.graphql(graphqlOperation(listTasks));
      const tasks = todoData.data.listTasks.items;
      const indexArr = [];
      const categories = [];
      tasks.forEach((item) => {
        if (indexArr.indexOf(item.category) < 0) {
          categories.push({category: item.category, id: item.id});
          indexArr.push(item.category);
        }
      });

      console.log('-----66666----');
      console.log(categories);
      console.log('-----66666----');
      setTodos(categories);
    } catch (err) {
      console.log(err);
      console.log('error fetching tasks');
    }
  }

  return (
    <FlatList
      data={tasks}
      numColumns={3}
      keyExtractor={(item) => item.id}
      renderItem={(item) => {
        return (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('SetTarget', {
                data: item,
              })
            }
            style={{
              flex: 1,
              borderColor: '#ddd',
              alignItems: 'center',
              justifyContent: 'center',
              borderWidth: 1,
              margin: 5,
              borderColor: '#ddd',
              borderRadius: 15,
              shadowColor: '#ddd',
            }}>
            <View>
              <Thumbnail large source={require('../../images/hisabee.png')} />
            </View>
            <View
              style={{
                borderTopColor: '#ddd',
                borderTopWidth: 1,
                paddingTop: 5,
              }}>
              <Text>{item?.item?.category}</Text>
            </View>
          </TouchableOpacity>
        );
      }}
    />
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default Hisabhome;
