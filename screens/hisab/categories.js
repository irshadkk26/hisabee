import React, {useState} from 'react';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
  Radio,
  Switch,
} from 'native-base';
import {Image, Dimensions, FlatList} from 'react-native';
import SegmentedControl from 'rn-segmented-control';
import {color} from 'react-native-reanimated';

export default function ListComponent(props) {
  const items = props.listData;

  const [tabIndex, setTabIndex] = React.useState(1);
  const [theme, setTheme] = React.useState('LIGHT');
  const toggleTheme = () =>
    theme === 'LIGHT' ? setTheme('DARK') : setTheme('LIGHT');
  const handleTabsChange = (index) => {
    setTabIndex(index);
  };

  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  return (
    <Container>
      {/* <Header>
          <Text>{this.props.header}</Text>
        </Header> */}
      <Content>
        {/* <Card>
            <CardItem header>
              <Text>NativeBase</Text>
            </CardItem>
            <CardItem>
              <Body>
                <View style={{display: 'flex', flexDirection: 'row'}}>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>jamaath</Text>
                  </View>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>no jama'ath</Text>
                  </View>
                  <View style={{display: 'flex', flexDirection: 'row'}}>
                    <CheckBox checked={true} />
                    <Text>no</Text>
                  </View>
                </View>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>GeekyAnts</Text>
            </CardItem>
          </Card> */}

        <FlatList
          style={{margin: 5}}
          data={items}
          numColumns={2}
          keyExtractor={(item) => item.index}
          renderItem={(item) => {
            return (
              <Card key={item.index} style={{flex: 1}}>
                <CardItem>
                  <Left>
                    <Thumbnail
                      source={require('../images/hisabee.png')}
                      style={{height: 20, width: 20}}
                    />
                    <Text style={{textTransform: 'capitalize'}}>
                      {item?.item?.name}?
                    </Text>
                  </Left>
                  {/* <Body style={{alignItems: 'flex-start'}}>
                    <Text>{item?.item?.name}?</Text>
                  </Body> */}
                  <Right>
                    <Switch
                      onValueChange={toggleSwitch}
                      value={item?.item?.switch}></Switch>
                  </Right>
                </CardItem>
              </Card>
            );
          }}
        />
      </Content>
    </Container>
  );
}
