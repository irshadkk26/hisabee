// ./screens/Contact.js

import React from 'react';
import {StyleSheet, SafeAreaView, ScrollView} from 'react-native';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
} from 'native-base';
import surahJson from '../resources/surah.json';
//http://api.aladhan.com/v1/calendar?latitude=25.2321057482617775&longitude=55.37811198694492&method=2&month=11&year=2020
const NamazTiming = ({navigation}) => {
  return (
    <ScrollView>
      <List>
        {surahJson.map((item, index) => {
          return (
            <ListItem thumbnail key={index}>
              <Left>
                <Thumbnail square source={require('../images/hisabee.png')} />
              </Left>
              <Body
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text numberOfLines={1}>{item.title}</Text>
                <Text>{item.titleAr}</Text>
              </Body>
              <Right>
                <Button
                  transparent
                  onPress={() =>
                    navigation.navigate('Surah', {
                      sura: index,
                    })
                  }>
                  <Text>View</Text>
                </Button>
              </Right>
            </ListItem>
          );
        })}
      </List>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default NamazTiming;
