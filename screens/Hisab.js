// ./screens/Home.js

import React, {useEffect, useState} from 'react';
import {
  View,
  Button,
  Text,
  StyleSheet,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import ListComponent from '../components/listcomponent';
// import {fetchTodos} from '../services';

import {API, graphqlOperation} from 'aws-amplify';

import {listTasks} from '../src/graphql/queries';
import {
  createTodo,
  updateTodo,
  deleteTodo,
  createTask,
} from '../src/graphql/mutations';

const Hisab = () => {
  const [tasks, setTodos] = useState([]);
  useEffect(() => {
    fetchTodos();
    // createTasks();
  }, []);
  async function fetchTodos() {
    try {
      const todoData = await API.graphql(graphqlOperation(listTasks));
      console.log('---------');
      console.log(todoData.data.listTasks.items);
      console.log('---------');
      const tasks = todoData.data.listTasks.items;
      setTodos(tasks);
    } catch (err) {
      console.log(err);
      console.log('error fetching tasks');
    }
  }

  async function createTasks() {
    const task = [
      {
        category: 'Quran',
        index: 1,
        description: 'Quran',
        subCategory: [
          {
            description: 'Study meaning &interpretations (Tafseer)',
            questions: [
              {
                question: 'Name of the surah',
                type: 'dropdown',
                valueFrom: 'surahList',
              },
              {
                question: 'Ayaths',
                type: 'range',
                valueFrom: 'surahAyathList',
              },
              {
                question: 'Targeted Date',
                type: 'datePicker',
              },
            ],
            subCategory: 'Study meaning &interpretations (Tafseer)',
            index: 2,
          },
          {
            description: 'Quran listening (Radios, podcast)',
            questions: [
              {
                question: 'Name of the surah',
                type: 'dropdown',
                valueFrom: 'surahList',
              },
              {
                question: 'Ayaths',
                type: 'range',
                valueFrom: 'surahAyathList',
              },
              {
                question: 'Targeted Date',
                type: 'datePicker',
              },
            ],
            subCategory: 'Quran listening (Radios, podcast)',
            index: 3,
          },
          {
            description: 'Recitation of quran',
            questions: [
              {
                question: 'Name of the surah',
                type: 'dropdown',
                valueFrom: 'surahList',
              },
              {
                question: 'Ayaths',
                type: 'range',
                valueFrom: 'surahAyathList',
              },
              {
                question: 'Targeted Date',
                type: 'datePicker',
              },
            ],
            subCategory: 'Recitation',
            index: 1,
          },
          {
            description: 'Hifl ul Quran ',
            questions: [
              {
                question: 'Name of the surah',
                type: 'dropdown',
                valueFrom: 'surahList',
              },
              {
                question: 'Ayaths',
                type: 'range',
                valueFrom: 'surahAyathList',
              },
              {
                question: 'Targeted Date',
                type: 'datePicker',
              },
            ],
            subCategory: 'Hifl ul Quran ',
            index: 4,
          },
        ],
      },
      {
        category: 'PRAYER',
        index: 2,
        description: 'PRAYER',
        subCategory: [
          {
            description: 'FARD PRAYER',
            questions: [
              {
                question: 'Fajr',
                type: 'withjamath',
              },
              {
                question: 'Dhuhr',
                type: 'withjamath',
              },
              {
                question: 'Asr',
                type: 'withjamath',
              },
              {
                question: 'Maghrib',
                type: 'withjamath',
              },
              {
                question: 'Isha',
                type: 'withjamath',
              },
            ],
            subCategory: 'FARD PRAYER',
            index: 1,
          },
          {
            description: 'RAVATHIB SUNNATH',
            questions: [
              {
                question: 'before Fajr',
                type: 'yesno',
              },
              {
                question: 'before Zuhr',
                type: 'yesno',
              },
              {
                question: 'after Maghrib',
                type: 'yesno',
              },
              {
                question: 'after Isha',
                type: 'yesno',
              },
            ],
            subCategory: 'RAVATHIB SUNNATH',
            index: 2,
          },
        ],
      },
    ];

    task.forEach(async (element) => {
      await API.graphql(graphqlOperation(createTask, {input: element}));
    });
  }

  if (tasks.length > 0) {
    // const fardhNamazes = [];
    // tasks.forEach((item) => {
    //   if (item.category === 'farz namaz') {
    //     fardhNamazes.push(item);
    //   }
    // });
    // const sunnahNamazes = [];
    // tasks.map((item) => {
    //   if (item.category === 'sunnah namaz') {
    //     sunnahNamazes.push(item);
    //   }
    // });
    return <ListComponent listData={tasks} header="IBADATHS"></ListComponent>;
  } else {
    return <Text>''</Text>;
  }
};

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
  },
});

export default Hisab;
