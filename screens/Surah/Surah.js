import styles from './styles';
import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {FlatList, SafeAreaView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
// import delay from 'delay';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  CheckBox,
  View,
  Card,
  CardItem,
  Icon,
} from 'native-base';

const RECORDS_PER_FETCH = 15;
const surahArr = [
  require('../../resources/surah/surah_1.json'),
  require('../../resources/surah/surah_2.json'),
  require('../../resources/surah/surah_3.json'),
  require('../../resources/surah/surah_4.json'),
  require('../../resources/surah/surah_5.json'),
  require('../../resources/surah/surah_6.json'),
  require('../../resources/surah/surah_7.json'),
  require('../../resources/surah/surah_8.json'),
  require('../../resources/surah/surah_9.json'),
  require('../../resources/surah/surah_10.json'),
  require('../../resources/surah/surah_11.json'),
  require('../../resources/surah/surah_12.json'),
  require('../../resources/surah/surah_13.json'),
  require('../../resources/surah/surah_14.json'),
  require('../../resources/surah/surah_15.json'),
  require('../../resources/surah/surah_16.json'),
  require('../../resources/surah/surah_17.json'),
  require('../../resources/surah/surah_18.json'),
  require('../../resources/surah/surah_19.json'),
  require('../../resources/surah/surah_20.json'),
  require('../../resources/surah/surah_21.json'),
  require('../../resources/surah/surah_22.json'),
  require('../../resources/surah/surah_23.json'),
  require('../../resources/surah/surah_24.json'),
  require('../../resources/surah/surah_25.json'),
  require('../../resources/surah/surah_26.json'),
  require('../../resources/surah/surah_27.json'),
  require('../../resources/surah/surah_28.json'),
  require('../../resources/surah/surah_29.json'),
  require('../../resources/surah/surah_30.json'),
  require('../../resources/surah/surah_31.json'),
  require('../../resources/surah/surah_32.json'),
  require('../../resources/surah/surah_33.json'),
  require('../../resources/surah/surah_34.json'),
  require('../../resources/surah/surah_35.json'),
  require('../../resources/surah/surah_36.json'),
  require('../../resources/surah/surah_37.json'),
  require('../../resources/surah/surah_38.json'),
  require('../../resources/surah/surah_39.json'),
  require('../../resources/surah/surah_40.json'),
  require('../../resources/surah/surah_41.json'),
  require('../../resources/surah/surah_42.json'),
  require('../../resources/surah/surah_43.json'),
  require('../../resources/surah/surah_44.json'),
  require('../../resources/surah/surah_45.json'),
  require('../../resources/surah/surah_46.json'),
  require('../../resources/surah/surah_47.json'),
  require('../../resources/surah/surah_48.json'),
  require('../../resources/surah/surah_49.json'),
  require('../../resources/surah/surah_50.json'),
  require('../../resources/surah/surah_51.json'),
  require('../../resources/surah/surah_52.json'),
  require('../../resources/surah/surah_53.json'),
  require('../../resources/surah/surah_54.json'),
  require('../../resources/surah/surah_55.json'),
  require('../../resources/surah/surah_56.json'),
  require('../../resources/surah/surah_57.json'),
  require('../../resources/surah/surah_58.json'),
  require('../../resources/surah/surah_59.json'),
  require('../../resources/surah/surah_60.json'),
  require('../../resources/surah/surah_61.json'),
  require('../../resources/surah/surah_62.json'),
  require('../../resources/surah/surah_63.json'),
  require('../../resources/surah/surah_64.json'),
  require('../../resources/surah/surah_65.json'),
  require('../../resources/surah/surah_66.json'),
  require('../../resources/surah/surah_67.json'),
  require('../../resources/surah/surah_68.json'),
  require('../../resources/surah/surah_69.json'),
  require('../../resources/surah/surah_70.json'),
  require('../../resources/surah/surah_71.json'),
  require('../../resources/surah/surah_72.json'),
  require('../../resources/surah/surah_73.json'),
  require('../../resources/surah/surah_74.json'),
  require('../../resources/surah/surah_75.json'),
  require('../../resources/surah/surah_76.json'),
  require('../../resources/surah/surah_77.json'),
  require('../../resources/surah/surah_78.json'),
  require('../../resources/surah/surah_79.json'),
  require('../../resources/surah/surah_80.json'),
  require('../../resources/surah/surah_81.json'),
  require('../../resources/surah/surah_82.json'),
  require('../../resources/surah/surah_83.json'),
  require('../../resources/surah/surah_84.json'),
  require('../../resources/surah/surah_85.json'),
  require('../../resources/surah/surah_86.json'),
  require('../../resources/surah/surah_87.json'),
  require('../../resources/surah/surah_88.json'),
  require('../../resources/surah/surah_89.json'),
  require('../../resources/surah/surah_90.json'),
  require('../../resources/surah/surah_91.json'),
  require('../../resources/surah/surah_92.json'),
  require('../../resources/surah/surah_93.json'),
  require('../../resources/surah/surah_94.json'),
  require('../../resources/surah/surah_95.json'),
  require('../../resources/surah/surah_96.json'),
  require('../../resources/surah/surah_97.json'),
  require('../../resources/surah/surah_98.json'),
  require('../../resources/surah/surah_99.json'),
  require('../../resources/surah/surah_100.json'),
  require('../../resources/surah/surah_101.json'),
  require('../../resources/surah/surah_102.json'),
  require('../../resources/surah/surah_103.json'),
  require('../../resources/surah/surah_104.json'),
  require('../../resources/surah/surah_105.json'),
  require('../../resources/surah/surah_106.json'),
  require('../../resources/surah/surah_107.json'),
  require('../../resources/surah/surah_108.json'),
  require('../../resources/surah/surah_109.json'),
  require('../../resources/surah/surah_110.json'),
  require('../../resources/surah/surah_111.json'),
  require('../../resources/surah/surah_112.json'),
  require('../../resources/surah/surah_113.json'),
  require('../../resources/surah/surah_114.json'),
];

export default function Surah({route, navigation}) {
  const {sura} = route.params;
  console.log(' in surah page with sura as ' + sura);
  const dispatch = useDispatch();
  const listItemsSurah = useSelector((state) => state.scrollmore.surahayaths);
  const totalItemsSurah = Array.isArray(listItemsSurah)
    ? listItemsSurah.length
    : 0;
  const [loadingMore, setLoadingMore] = useState(false);
  const [allLoaded, setAllLoaded] = useState(false);

  const surahJson = surahArr[sura];
  const verses = surahJson?.verse;
  const versesArr = [];
  var i = 0;
  for (const key in verses) {
    if (verses.hasOwnProperty(key)) {
      versesArr.push({index: i, verse: verses[key]});
    }
  }
  const fetchResults = (versesArr, startingId = 0) => {
    let obj = [];

    for (let i = startingId; i < startingId + RECORDS_PER_FETCH; i++) {
      if (versesArr[i] === undefined) break;

      obj.push(versesArr[i]);
    }
    return obj;
  };

  useEffect(() => {
    initialiseList();
  }, []);

  const initialiseList = async () => {
    // this is done for testing purposes - reset AsyncStorage on every app refresh
    await AsyncStorage.removeItem('saved_list');

    // get current persisted list items (will be null if above line is not removed)
    const curItems = await AsyncStorage.getItem('saved_list');

    if (curItems === null) {
      // no current items in AsyncStorage - fetch initial items
      json = fetchResults(versesArr, 0);

      // set initial list in AsyncStorage
      await AsyncStorage.setItem('saved_list', JSON.stringify(json));
    } else {
      // current items exist - format as a JSON object
      json = JSON.parse(curItems);
    }

    // update Redux store (Redux will ignore if `json` is same as current list items)
    dispatch({
      type: 'UPDATE_LIST_RESULTS_SURAH',
      items: json,
    });
  };

  const persistResults = async (newItems) => {
    // get current persisted list items
    const curItems = await AsyncStorage.getItem('saved_list');

    // format as a JSON object
    let json = curItems === null ? {} : JSON.parse(curItems);

    // add new items to json object
    for (let item of newItems) {
      json.push(item);
    }

    // persist updated item list
    await AsyncStorage.setItem('saved_list', JSON.stringify(json));

    // update Redux store
    dispatch({
      type: 'UPDATE_LIST_RESULTS_SURAH',
      items: json,
    });
  };

  const loadMoreResults = async (info) => {
    // if already loading more, or all loaded, return
    if (loadingMore || allLoaded) return;

    // set loading more (also updates footer text)
    setLoadingMore(true);

    // get next results
    const newItems = fetchResults(versesArr, totalItemsSurah);

    // mimic server-side API request and delay execution for 1 second
    // await delay(1000);

    if (newItems.length === 0) {
      // if no new items were fetched, set all loaded to true to prevent further requests
      setAllLoaded(true);
    } else {
      // process the newly fetched items
      await persistResults(newItems);
    }

    // load more complete, set loading more to false
    setLoadingMore(false);
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      {listItemsSurah && (
        <FlatList
          contentContainerStyle={styles.list}
          ListHeaderComponent={
            <View style={styles.header}>
              <Text style={styles.title}>
                Displaying {totalItemsSurah} Items
              </Text>
            </View>
          }
          ListFooterComponent={
            <View style={styles.footer}>
              {loadingMore && (
                <Text style={styles.footerText}>Loading More...</Text>
              )}
            </View>
          }
          scrollEventThrottle={250}
          onEndReached={(info) => {
            loadMoreResults(info);
          }}
          onEndReachedThreshold={0.01}
          data={listItemsSurah}
          keyExtractor={(item) => 'item_' + item.verse}
          renderItem={({item, index}) => {
            return (
              // <React.Fragment key={index}>
              //   <View style={styles.item}>
              //     <Text>Item </Text>
              //   </View>
              // </React.Fragment>
              <ListItem thumbnail key={index}>
                <Left>
                  <Thumbnail
                    square
                    source={require('../../images/hisabee.png')}
                  />
                </Left>
                <Body>
                  <Text style={{direction: 'ltr', textAlign: 'right'}}>
                    {item.verse}
                  </Text>
                  {/* <Text note numberOfLines={1}>
                  {item.title}
                </Text> */}
                </Body>
                <Right>
                  <Button transparent>
                    <Text>View</Text>
                  </Button>
                </Right>
              </ListItem>
            );
          }}
        />
      )}
    </SafeAreaView>
  );
}
