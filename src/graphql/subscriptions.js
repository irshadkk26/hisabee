/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTask = /* GraphQL */ `
  subscription OnCreateTask {
    onCreateTask {
      id
      category
      index
      description
      subCategory {
        description
        subCategory
        index
        questions {
          question
          type
          valueFrom
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const onUpdateTask = /* GraphQL */ `
  subscription OnUpdateTask {
    onUpdateTask {
      id
      category
      index
      description
      subCategory {
        description
        subCategory
        index
        questions {
          question
          type
          valueFrom
        }
      }
      createdAt
      updatedAt
    }
  }
`;
export const onDeleteTask = /* GraphQL */ `
  subscription OnDeleteTask {
    onDeleteTask {
      id
      category
      index
      description
      subCategory {
        description
        subCategory
        index
        questions {
          question
          type
          valueFrom
        }
      }
      createdAt
      updatedAt
    }
  }
`;
