import {API, graphqlOperation} from 'aws-amplify';
import {createTodo} from './src/graphql/mutations';
import {listTodos} from './src/graphql/queries';

export async function fetchTodos() {
  try {
    const todoData = await API.graphql(graphqlOperation(listTodos));
    const todos = todoData.data.listTodos.items;
    return todos;
  } catch (err) {
    console.log('error fetching todos');
  }
}

async function addTodo() {
  try {
    const todo = {...formState};
    setTodos([...todos, todo]);
    setFormState(initialState);
    await API.graphql(graphqlOperation(createTodo, {input: todo}));
  } catch (err) {
    console.log('error creating todo:', err);
  }
}
